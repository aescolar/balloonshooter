package com.autlos.balloonshooter;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.RelativeLayout;

import com.badlogic.gdx.backends.android.AndroidApplication;
import com.google.ads.AdRequest;
import com.google.ads.AdSize;
import com.google.ads.AdView;
import com.swarmconnect.Swarm;

@SuppressLint("HandlerLeak")
public class MainActivity extends AndroidApplication implements IActivityRequestHandler {
	private final int SHOW_ADS = 1;
	private final int HIDE_ADS = 0;

	private boolean swarmActive;

	// Admob banner:
	protected AdView adView;

	protected Handler handler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case SHOW_ADS: {
				adView.setVisibility(View.VISIBLE);
				break;
			}
			case HIDE_ADS: {
				adView.setVisibility(View.GONE);
				break;
			}
			}
		}
	};

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// Create the layout
		RelativeLayout layout = new RelativeLayout(this);

		// If swarm is going to be used:
		swarmActive = true;

		// Swarm
		if (swarmActive) {
			Swarm.setActive(this);
			Swarm.init(this, 5005, "5cf00784ccd6224e1a06b9fe1f44da96");
		}

		// Do the stuff that initialize() would do for you
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);

		// Create the libgdx View
		View gameView = initializeForView(new BalloonShooter(this, swarmActive), true);

		// Create and setup the AdMob view
		adView = new AdView(this, AdSize.BANNER, "a1513656276a42d"); // Put in your secret key here
		adView.loadAd(new AdRequest());
		// Add the libgdx view
		layout.addView(gameView);

		// Add the AdMob view
		RelativeLayout.LayoutParams adParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT,
		      RelativeLayout.LayoutParams.WRAP_CONTENT);
		adParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);

		layout.addView(adView, adParams);

		// Hook it all up
		setContentView(layout);

	}

	public void onResume() {
		super.onResume();
		if (swarmActive) {
			Swarm.setActive(this);
		}
	}

	public void onPause() {
		super.onPause();
		if (swarmActive) {
			Swarm.setInactive(this);
		}
	}

	@Override
	public void showAds(boolean show) {
		handler.sendEmptyMessage(show ? SHOW_ADS : HIDE_ADS);
	}

}