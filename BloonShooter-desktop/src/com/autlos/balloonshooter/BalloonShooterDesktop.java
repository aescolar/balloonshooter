package com.autlos.balloonshooter;

import com.autlos.balloonshooter.BalloonShooter;
import com.autlos.balloonshooter.IActivityRequestHandler;
import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;

public class BalloonShooterDesktop implements IActivityRequestHandler {
	private static BalloonShooterDesktop application;
	public static void main(String[] args) {
		LwjglApplicationConfiguration cfg = new LwjglApplicationConfiguration();
		cfg.title = "Balloon Shooter";
		cfg.useGL20 = false;
		cfg.width = 480;
		cfg.height = 800;
		cfg.resizable = false;
		
		  if (application == null) {
           application = new BalloonShooterDesktop();
       }
		
		new LwjglApplication(new BalloonShooter(application, false), cfg);
	}
	
	@Override
   public void showAds(boolean show) {
	   
   }

}
