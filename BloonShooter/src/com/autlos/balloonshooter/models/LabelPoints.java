package com.autlos.balloonshooter.models;

import com.autlos.balloonshooter.Assets;
import com.autlos.balloonshooter.BalloonShooter;
import com.autlos.sgf.BasicLabel;
import com.badlogic.gdx.math.Vector2;

public class LabelPoints extends BasicLabel {

	private float lifeTime = 0.5f;
	private float currentTime = 0f;
	private float SPEED = 120f*BalloonShooter.scaleY;
	public LabelPoints(int points, Vector2 position) {
		super(Assets.font, "" + points, position);
	}
	
	public void update(float delta){
		currentTime += delta;
		position.y += SPEED*delta;
	}
	
	public boolean isFinished(){
		return currentTime >= lifeTime;
	}

}
