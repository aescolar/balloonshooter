package com.autlos.balloonshooter.models.enemies;

import com.autlos.balloonshooter.Assets;
import com.badlogic.gdx.math.Vector2;

public class BalloonSmall extends Balloon {

	public BalloonSmall(Vector2 position, float SPEED) {
	   super(Assets.smallBalloon, position, SPEED, Assets.smallBalloon.getRegionWidth()/2*0.5f);
		this.points = 300;
   }
	
	public void update(){
		super.update();
	}
  

}
