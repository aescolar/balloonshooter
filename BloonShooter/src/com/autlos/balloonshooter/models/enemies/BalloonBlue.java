package com.autlos.balloonshooter.models.enemies;

import com.autlos.balloonshooter.Assets;
import com.badlogic.gdx.math.Vector2;

public class BalloonBlue extends Balloon{

	public BalloonBlue(Vector2 position, float SPEED) {
	   super(Assets.blueBalloon, position, SPEED, 64f);
	   this.points = 0;
   }

}
