package com.autlos.balloonshooter.models.enemies;

import com.autlos.balloonshooter.Assets;
import com.badlogic.gdx.math.Vector2;

public class BalloonRed extends Balloon {

	public BalloonRed(Vector2 position, float SPEED) {
	   super(Assets.redBalloon, position, SPEED, 64);
	   this.points = 100;
   }
	
	public void update(){
		super.update();
	}

}
