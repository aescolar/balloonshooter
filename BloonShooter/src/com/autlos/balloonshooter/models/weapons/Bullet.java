package com.autlos.balloonshooter.models.weapons;

import com.autlos.balloonshooter.BalloonShooter;
import com.autlos.sgf.models.BasicProjectile;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;

public class Bullet extends BasicProjectile{

	public Bullet(TextureRegion texture, float SPEED, Vector2 position, float rotation) {
		super(texture, position, SPEED, rotation);
		setScale(BalloonShooter.scaleX, BalloonShooter.scaleX);
		createBounds(2f, 2f);
   }
		
}
