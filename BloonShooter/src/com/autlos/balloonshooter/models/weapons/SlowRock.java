package com.autlos.balloonshooter.models.weapons;

import com.autlos.balloonshooter.BalloonShooter;
import com.autlos.sgf.models.BasicProjectile;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;


public class SlowRock extends BasicProjectile {
	private int kills;
	
	/**
	 * 
	 * @param texture
	 * @param SPEED
	 * @param position
	 * @param rotation
	 */
	public SlowRock(TextureRegion texture, float SPEED, Vector2 position, float rotation) {
		super(texture, position, SPEED, rotation);
		setScale(BalloonShooter.scaleX, BalloonShooter.scaleX);
		createBounds(5f, 5f);
		this.kills = 0;
   }

	
	public int getKills(){
		return kills;
	}
	
	public void addKill(){
		kills++;
	}
}
