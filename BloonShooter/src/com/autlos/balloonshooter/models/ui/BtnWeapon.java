package com.autlos.balloonshooter.models.ui;

import com.autlos.balloonshooter.Assets;
import com.autlos.balloonshooter.BalloonShooter;
import com.autlos.sgf.GuiElement;
import com.badlogic.gdx.math.Vector2;

public class BtnWeapon extends GuiElement {

	public BtnWeapon() {
		super(Assets.btnWeapon, new Vector2(15f * BalloonShooter.scaleX, 95f * BalloonShooter.scaleY), 1, 2,
		      BalloonShooter.scaleX, BalloonShooter.scaleY);
		createBounds(-15f, -12f);
	}

	// public BtnWeapon() {
	// super(Assets.btnWeapon, new Vector2(15f * BalloonShooter.SCALE, 95f * BalloonShooter.SCALE), 64,
	// BalloonShooter.SCALE);
	// this.bounds = new Rectangle(sprite.getX() - 17.0f * BalloonShooter.SCALE, sprite.getY() - 20.0f
	// * BalloonShooter.SCALE, sprite.getWidth() + 34.0f * BalloonShooter.SCALE, sprite.getHeight() + 28.0f
	// * BalloonShooter.SCALE);
	// }

}
