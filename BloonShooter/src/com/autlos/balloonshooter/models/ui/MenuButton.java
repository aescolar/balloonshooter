package com.autlos.balloonshooter.models.ui;

import com.autlos.balloonshooter.Assets;
import com.autlos.balloonshooter.BalloonShooter;
import com.autlos.sgf.GuiTextElement;
import com.badlogic.gdx.math.Vector2;

public class MenuButton extends GuiTextElement{
	public MenuButton (String text){
		super(Assets.btnMenu, Assets.font, text, 2, 1);
		setScale(BalloonShooter.scaleX, BalloonShooter.scaleY);
	}
	
	public void touch(){
		this.setState(State.PRESSED);
		// TODO: play sound here
	}
	
	public void setPosition(Vector2 position){
		super.setPosition(position);
		createBounds();
	}

	public void setPosition(float x, float y){
		super.setPosition(x, y);
		createBounds();
	}
}
