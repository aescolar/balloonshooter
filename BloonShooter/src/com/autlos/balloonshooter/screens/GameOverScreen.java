package com.autlos.balloonshooter.screens;

import com.autlos.balloonshooter.Assets;
import com.autlos.balloonshooter.BalloonFactory.DifficultyMode;
import com.autlos.balloonshooter.BalloonShooter;
import com.autlos.balloonshooter.Sounds;
import com.autlos.balloonshooter.models.ui.MenuButton;
import com.autlos.balloonshooter.view.GameWorld;
import com.autlos.balloonshooter.view.World;
import com.autlos.sgf.BasicLabel;
import com.autlos.sgf.GameAbstract;
import com.autlos.sgf.ScreenAbstract;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;

public class GameOverScreen extends ScreenAbstract {
	GameWorld world;
	BalloonShooter game;

	private BasicLabel labelGameOver;
	private BasicLabel labelHighScore;
	private BasicLabel labelScore;
	private MenuButton btnRestart;
	private MenuButton btnBack;

	private long record;
	private long score;

	private float minTimeScreen = 0.2f;
	private float currentTimeScreen = 0f;
	private float transitionCurrentTime;
	private float transitionStateTime = 0.25f;
	private boolean activated = false;

	public GameOverScreen(World world) {
		super(Assets.background);

		this.world = (GameWorld) world;
		this.game = world.getGame();
		game.catchBackButton(true);

		if (BalloonShooter.soundState != Sounds.MUTED) {
			game.sounds.startMusic(0.6f);
		}

		this.score = world.getScore();
		record = world.getHighScore() > score ? world.getHighScore() : score;
		game.setAdsVisible(true);

		Gdx.input.setInputProcessor(null);
	}

	@Override
	public void render(float delta) {
		super.render(delta);

		if (activated) {
			transitionCurrentTime += delta;
			if (transitionCurrentTime >= transitionStateTime) {
				dispose();
				if (btnBack.isPressed()) {
					game.setScreen(new MainMenuScreen(game));
				} else if (btnRestart.isPressed()) {
					game.setScreen(new GameScreen(game, world.getMode()));
				}
			}
		}

		batch.begin();
		labelGameOver.draw(batch);
		labelHighScore.draw(batch);
		labelScore.draw(batch);
		btnRestart.draw(batch);
		btnBack.draw(batch);
		batch.end();

		if (currentTimeScreen < minTimeScreen) {
			currentTimeScreen += delta;
		} else {
			manageInput();
		}

	}

	private void manageInput() {
		if (Gdx.input.isKeyPressed(Keys.BACK)) {
			game.setScreen(new MainMenuScreen(game));
		}
		if (Gdx.input.justTouched()) {
			Vector2 posTouched = new Vector2();
			posTouched.x = Gdx.input.getX();
			posTouched.y = GameAbstract.screenHeight - Gdx.input.getY();
			if (!activated) {
				if (btnRestart.isTouchingElement(posTouched)) {
					btnRestart.touch();
					activated = true;
				} else if (btnBack.isTouchingElement(posTouched)) {
					btnBack.touch();
					activated = true;
				}
			}
		}
	}

	@Override
	public void resize(int width, int height) {
		labelGameOver = new BasicLabel(Assets.font, "GAME OVER");
		labelGameOver.setPosition(BalloonShooter.screenMidX - labelGameOver.getWidth() / 2, BalloonShooter.screenHeight
		      - 3 * BalloonShooter.lblSeparationY);

		labelHighScore = new BasicLabel(Assets.font, "RECORD: ", record);
		labelHighScore.setPosition(BalloonShooter.screenMidX - labelHighScore.getWidth() / 2,
		      labelGameOver.getPosition().y - BalloonShooter.lblSeparationY);

		labelScore = new BasicLabel(Assets.font, "SCORE: ", score);
		labelScore.setPosition(BalloonShooter.screenMidX - labelScore.getWidth() / 2, labelHighScore.getPosition().y
		      - BalloonShooter.lblSeparationY);

		btnRestart = new MenuButton("RESTART");
		btnRestart.setPosition(BalloonShooter.screenMidX - btnRestart.getOrigin().x, labelScore.getPosition().y
		      - btnRestart.getHeight() - BalloonShooter.lblSeparationY);

		btnBack = new MenuButton("BACK");
		btnBack.setPosition(BalloonShooter.screenMidX - btnBack.getOrigin().x,
		      btnRestart.getPosition().y - btnBack.getHeight() - BalloonShooter.lblSeparationY);

	}

	@Override
	public void show() {
		batch = new SpriteBatch();
	}

	@Override
	public void hide() {

	}

	@Override
	public void pause() {

	}

	@Override
	public void resume() {

	}

	@Override
	public void dispose() {
		batch.dispose();

		game.getData().setContballoons();
		if (score >= 1000) {
			game.getData().addContGames();
		}
		if (world.getMode() == DifficultyMode.HARD) {
			game.getData().setHighScoreHard(score);

		} else if (world.getMode() == DifficultyMode.NORMAL) {
			game.getData().setHighScoreNormal(score);
		} else {
			game.getData().setHighScoresEasy(score);
		}

		game.getData().writeScores();

		world.dispose();

	}

}
