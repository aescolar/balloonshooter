package com.autlos.balloonshooter.screens;

import aurelienribon.tweenengine.BaseTween;
import aurelienribon.tweenengine.Tween;
import aurelienribon.tweenengine.TweenCallback;
import aurelienribon.tweenengine.TweenEquations;
import aurelienribon.tweenengine.TweenManager;

import com.autlos.balloonshooter.Assets;
import com.autlos.balloonshooter.BalloonShooter;
import com.autlos.balloonshooter.tweenaccessors.SpriteTween;
import com.autlos.sgf.GameAbstract.TARGET_SIZE;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class LogoScreen implements Screen {

	
	private Sprite splashSprite;
	private SpriteBatch batch;
	private BalloonShooter game;

	private TweenManager tweenManager;

	public LogoScreen(BalloonShooter game) {
		this.game = game;
	}

	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
		
		tweenManager.update(delta);
		batch.begin();
		splashSprite.draw(batch);
		batch.end();

	}

	@Override
	public void resize(int width, int height) {

	}

	@Override
	public void show() {
		
		splashSprite = new Sprite(Assets.splashTexture);
		splashSprite.setSize(Assets.splashTexture.getRegionWidth()*BalloonShooter.scaleX, Assets.splashTexture.getRegionHeight()*BalloonShooter.scaleY);
		
		if(BalloonShooter.targetSize == TARGET_SIZE.BIG){
			splashSprite.setScale(BalloonShooter.SCALE_X_BIG, BalloonShooter.SCALE_Y_BIG);
		}else{
			splashSprite.setScale(BalloonShooter.SCALE_X_SMALL, BalloonShooter.SCALE_Y_SMALL);
		}
		
		splashSprite.setOrigin(splashSprite.getWidth() / 2,
				splashSprite.getHeight() / 2);
		splashSprite.setPosition(
				Gdx.graphics.getWidth() / 2 - splashSprite.getWidth() / 2,
				Gdx.graphics.getHeight() / 2 - splashSprite.getHeight() / 2);
		splashSprite.setColor(1, 1, 1, 0);

		batch = new SpriteBatch();
		Tween.registerAccessor(Sprite.class, new SpriteTween());
		tweenManager = new TweenManager();

		TweenCallback cb = new TweenCallback() {

			@Override
			public void onEvent(int type, BaseTween<?> source) {
				tweenCompleted();
			}

		};

		Tween.to(splashSprite, SpriteTween.ALPHA, 2.5f).target(1)
				.ease(TweenEquations.easeInQuad).repeatYoyo(1, 1.5f).setCallback(cb)
				.setCallbackTriggers(TweenCallback.COMPLETE)
				.start(tweenManager);
	}

	private void tweenCompleted() {
		game.setScreen(new MainMenuScreen(game));
	}

	@Override
	public void hide() {

	}

	@Override
	public void pause() {

	}

	@Override
	public void resume() {

	}

	@Override
	public void dispose() {
		batch.dispose();
	}

}
