package com.autlos.balloonshooter.screens;

import com.autlos.balloonshooter.Assets;
import com.autlos.balloonshooter.BalloonFactory.DifficultyMode;
import com.autlos.balloonshooter.BalloonShooter;
import com.autlos.balloonshooter.models.ui.MenuButton;
import com.autlos.sgf.BasicLabel;
import com.autlos.sgf.GameAbstract;
import com.autlos.sgf.GuiElement.State;
import com.autlos.sgf.ScreenAbstract;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.math.Vector2;

public class LevelSelectionScreen extends ScreenAbstract {
	// private SpriteBatch batch;
	private BalloonShooter game;

	private BasicLabel lblTitle;
	private MenuButton btnEasy;
	private MenuButton btnNormal;
	private MenuButton btnHard;
	private MenuButton btnBack;

	//
	private float minTimeScreen = 0.25f;
	private float currentTimeScreen = 0f;
	private float transitionCurrentTime;
	private float transitionStateTime = 0.2f;
	private boolean activated = false;

	public LevelSelectionScreen(BalloonShooter game) {
		super(Assets.background);
		this.game = game;
		game.setAdsVisible(true);
		
		game.catchBackButton(true);
	}

	@Override
	public void render(float delta) {
		super.render(delta);
		
		if(activated){
			transitionCurrentTime += delta;
			if(transitionCurrentTime >= transitionStateTime){
				if(btnEasy.getState() == State.PRESSED){
					game.setScreen(new GameScreen(game, DifficultyMode.EASY));
				}else if(btnNormal.getState() == State.PRESSED){
					game.setScreen(new GameScreen(game, DifficultyMode.NORMAL));
				}else if(btnHard.getState() == State.PRESSED){
					game.setScreen(new GameScreen(game, DifficultyMode.HARD));
				}else{
					game.setScreen(new MainMenuScreen(game));
				}
			}
		}

		batch.begin();

		lblTitle.draw(batch);
		btnEasy.draw(batch);
		btnNormal.draw(batch);
		btnHard.draw(batch);
		btnBack.draw(batch);

		batch.end();

		if (currentTimeScreen >= minTimeScreen) {
			manageInput();
		} else {
			currentTimeScreen += delta;
		}

	}

	private void manageInput() {
		if (Gdx.input.isKeyPressed(Keys.BACK)) {
			game.setScreen(new MainMenuScreen(game));
		}
		if (Gdx.input.justTouched()) {
			Vector2 posTouched = new Vector2();
			posTouched.x = Gdx.input.getX();
			posTouched.y = GameAbstract.screenHeight - Gdx.input.getY();
			if (!activated) {
				if (btnEasy.isTouchingElement(posTouched)) {
					btnEasy.touch();
					activated = true;
				} else if (btnNormal.isTouchingElement(posTouched)) {
					btnNormal.touch();
					activated = true;
				} else if (btnHard.isTouchingElement(posTouched)) {
					btnHard.touch();
					activated = true;
				} else if(btnBack.isTouchingElement(posTouched)){
					btnBack.touch();
					activated = true;
				}
			}
		}
	}

	@Override
	public void resize(int width, int height) {
		lblTitle = new BasicLabel(Assets.font, "SELECT MODE:");
		lblTitle.setPosition(BalloonShooter.screenMidX - lblTitle.getWidth() / 2, BalloonShooter.screenHeight - 3
		      * BalloonShooter.lblSeparationY);

		btnEasy = new MenuButton("EASY");
		btnEasy.setPosition(BalloonShooter.screenMidX - btnEasy.getOrigin().x,
		      lblTitle.getPosition().y - btnEasy.getHeight() - BalloonShooter.lblSeparationY);

		btnNormal = new MenuButton("NORMAL");
		btnNormal.setPosition(BalloonShooter.screenMidX - btnNormal.getOrigin().x,
		      btnEasy.getPosition().y - btnNormal.getHeight() - BalloonShooter.lblSeparationY);

		btnHard = new MenuButton("HARD");
		btnHard.setPosition(BalloonShooter.screenMidX - btnHard.getOrigin().x,
		      btnNormal.getPosition().y - btnHard.getHeight() - BalloonShooter.lblSeparationY);

		btnBack = new MenuButton("BACK");
		btnBack.setPosition(BalloonShooter.screenMidX - btnBack.getOrigin().x,
		      btnHard.getPosition().y - btnBack.getHeight() - BalloonShooter.lblSeparationY);

	}

	@Override
	public void show() {
	}

	@Override
	public void hide() {

	}

	@Override
	public void pause() {

	}

	@Override
	public void resume() {

	}

	@Override
	public void dispose() {
		super.dispose();
	}
}
