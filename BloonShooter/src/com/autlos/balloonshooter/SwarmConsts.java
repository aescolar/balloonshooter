package com.autlos.balloonshooter;

public class SwarmConsts {

	public static class App {
		public static final int APP_ID = 5005;
		public static final String APP_AUTH = "5cf00784ccd6224e1a06b9fe1f44da96";
	}

	public static class Leaderboard {
		public static final int HIGH_SCORES_NORMAL_ID = 7303;
		public static final int HIGH_SCORES_HARD_ID = 7305;
		public static final int BALLOONS_COUNT_ID = 7311;
		public static final int HIGH_SCORES_EASY_ID = 7805;
	}

	public static class Achievement {
		public static final int BRONZE_KILLER_ID = 11425;
		public static final int SILVER_KILLER_ID = 11437;
		public static final int GOLD_KILLER_ID = 11439;
		public static final int DIAMOND_KILLER_ID = 11443;
		public static final int LUCK_OR_PRECISION_I_ID = 11431;
		public static final int LUCK_OR_PRECISION_II_ID = 11433;
		public static final int LUCK_OR_PRECISION_III_ID = 11435;
		public static final int SURVIVOR_LVL_I_ID = 11901;
		public static final int SURVIVOR_LVL_II_ID = 11903;
		public static final int SURVIVOR_LVL_III_ID = 11905;
		public static final int EASY_APPRENTICE_ID = 11893;
		public static final int EASY_AMATEUR_ID = 11895;
		public static final int EASY_VETERAN_ID = 11897;
		public static final int EASY_MASTER_ID = 11899;
		public static final int NORMAL_APPRENTICE_ID = 11455;
		public static final int NORMAL_AMATEUR_ID = 11457;
		public static final int NORMAL_VETERAN_ID = 11493;
		public static final int NORMAL_MASTER_ID = 11459;
		public static final int HARD_APPRENTICE_ID = 11461;
		public static final int HARD_AMATEUR_ID = 11463;
		public static final int HARD_VETERAN_ID = 11465;
		public static final int HARD_MASTER_ID = 11495;
		public static final int CONSTANT_I_ID = 11441;
		public static final int CONSTANT_II_ID = 11445;
		public static final int CONSTANT_III_ID = 11447;
		public static final int TURTLE_ID = 11449;
		public static final int BALLOON_MANIAC_I_ID = 11497;
		public static final int BALLOON_MANIAC_II_ID = 11451;
		public static final int BALLOON_MANIAC_III_ID = 11499;
		public static final int ROCK_PROFICENCY_ID = 11471;
	}

	public static class StoreCategory {
	}

	public static class StoreItem {
	}

	public static class StoreItemListing {
	}

}