package com.autlos.balloonshooter;

import com.autlos.balloonshooter.models.Bonus;
import com.autlos.balloonshooter.models.enemies.Balloon;
import com.autlos.balloonshooter.models.enemies.BalloonBlue;
import com.autlos.balloonshooter.models.enemies.BalloonGreen;
import com.autlos.balloonshooter.models.enemies.BalloonRed;
import com.autlos.balloonshooter.models.enemies.BalloonSmall;
import com.autlos.sgf.Timer;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;

public class BalloonFactory {

	public static enum DifficultyMode {
		EASY, NORMAL, HARD
	}

	public enum BalloonType {
		RED, BLUE, SMALL, GREEN
	}

	private DifficultyMode mode;

	// Bonus constants:
	private float MIN_POS_Y_BONUS;
	private float MAX_POS_Y_BONUS;
	private float BONUS_SPEED;

	// One timer for each balloon:
	Timer greenTimer;
	Timer redTimer;
	Timer smallTimer;
	Timer blueTimer;

	// Speeds for each balloon:
	private float RED_SPEED;
	private float GREEN_SPEED;
	private float SMALL_SPEED;
	private float BLUE_SPEED;

	// Times for each balloon:
	private float minTimeRed;
	private float maxTimeRed;
	private float minTimeGreen;
	private float maxTimeGreen;
	private float minTimeSmall;
	private float maxTimeSmall;
	private float minTimeBlue;
	private float maxTimeBlue;

	// stateTime measures the time between every difficulty increase
	private float stateTime;
	// currentTime measures the time since the last difficulty update.
	private float currentTime;
	// Just to increase the speed each 2 difficulty updates.
	private int contUpdates;
	// This way the smallBalloon speed and frequency only updates after one has been created.
	private boolean isSmallBalloon;

	// Balloon's limit coordinates:
	private float maxXRight;
	private float minXRight;
	private float maxXIzq;
	// position on the Y axis where the balloons are created:
	private float posY = 5;

	Array<Balloon> balloons;

	public BalloonFactory(DifficultyMode df) {
		this.mode = df;
		// Sets the start times and speeds for EVERYTHING:
		if (this.mode == DifficultyMode.HARD) {
			minTimeRed = 1.8f;
			maxTimeRed = 2.4f;
			minTimeGreen = 6.5f;
			maxTimeGreen = 10.0f;
			minTimeBlue = 10f;
			maxTimeBlue = 16f;
			minTimeSmall = 14f;
			maxTimeSmall = 20f;
			RED_SPEED = Gdx.graphics.getHeight() / 14.2f;
			GREEN_SPEED = Gdx.graphics.getHeight() / 11.0f;
			SMALL_SPEED = Gdx.graphics.getHeight() / 13.2f;
			BLUE_SPEED = Gdx.graphics.getHeight() / 17.4f;

			BONUS_SPEED = Gdx.graphics.getWidth() / 5.2f;

			stateTime = 12f;
		} else if (this.mode == DifficultyMode.NORMAL) {
			minTimeRed = 2.5f;
			maxTimeRed = 3.3f;
			minTimeGreen = 10.0f;
			maxTimeGreen = 14f;
			minTimeBlue = 14f;
			maxTimeBlue = 18f;
			minTimeSmall = 18f;
			maxTimeSmall = 23f;

			RED_SPEED = Gdx.graphics.getHeight() / 16.6f;
			GREEN_SPEED = Gdx.graphics.getHeight() / 13.4f;
			SMALL_SPEED = Gdx.graphics.getHeight() / 15.6f;
			BLUE_SPEED = Gdx.graphics.getHeight() / 18.8f;

			BONUS_SPEED = Gdx.graphics.getWidth() / 5.5f;

			stateTime = 14f;
		} else if (this.mode == DifficultyMode.EASY) {
			minTimeRed = 2.7f;
			maxTimeRed = 3.5f;
			minTimeGreen = 12.0f;
			maxTimeGreen = 16.0f;
			minTimeBlue = 16f;
			maxTimeBlue = 20f;
			minTimeSmall = 20f;
			maxTimeSmall = 25f;

			RED_SPEED = Gdx.graphics.getHeight() / 17.2f;
			GREEN_SPEED = Gdx.graphics.getHeight() / 14.0f;
			SMALL_SPEED = Gdx.graphics.getHeight() / 16.2f;
			BLUE_SPEED = Gdx.graphics.getHeight() / 19.5f;

			BONUS_SPEED = Gdx.graphics.getWidth() / 5.7f;

			stateTime = 15f;
		}

		balloons = new Array<Balloon>();

		// Set the update values:
		contUpdates = 0;
		currentTime = 0f;
		isSmallBalloon = false;

		// Create the timers with a Random time:
		greenTimer = new Timer(MathUtils.random(minTimeGreen, maxTimeGreen));
		redTimer = new Timer((MathUtils.random(minTimeRed, maxTimeRed)));
		smallTimer = new Timer(MathUtils.random(minTimeSmall, maxTimeSmall));
		blueTimer = new Timer(MathUtils.random(minTimeBlue, maxTimeBlue));

		// Set the limit coordinates:
		maxXIzq = Gdx.graphics.getWidth() / 2 - 90f * BalloonShooter.scaleX;
		minXRight = Gdx.graphics.getWidth() / 2 + 20f * BalloonShooter.scaleX;
		maxXRight = Gdx.graphics.getWidth() - 70f * BalloonShooter.scaleX;

		// Limit for the random coordinates for the bonuses:
		MIN_POS_Y_BONUS = Gdx.graphics.getHeight() / 1.75f;
		MAX_POS_Y_BONUS = Gdx.graphics.getHeight() - 64 * BalloonShooter.scaleY;
	}

	/**
	 * Create balloons and updates the difficulty
	 */
	public void update(float delta) {
		greenTimer.update(delta);
		redTimer.update(delta);
		smallTimer.update(delta);
		blueTimer.update(delta);

		// Increase the currentTime since last difficulty update.
		currentTime = currentTime + delta;
		// If it's time to update, it checks the difficulty mode and update Speeds and times:
		if (stateTime <= currentTime) {
			if (mode == DifficultyMode.HARD) {
				updateTimesHard();
			} else if (mode == DifficultyMode.NORMAL) {
				updateTimesNormal();
			} else if (mode == DifficultyMode.EASY) {
				updateTimesEasy();
			}
			currentTime = 0;
			// To make the difficulty increase slower over the time (values are hard-coded and balanced by testing):
			if (stateTime <= 20f) {
				stateTime = stateTime * 1.14f;
			}
		}

		// Check if one of the timers has timeElapsed, if it has, create a balloon, add it to the array, and reset the timer
		if (greenTimer.hasTimeElapsed()) {
			balloons.add(createBalloon(BalloonType.GREEN));
			greenTimer.reset(MathUtils.random(minTimeGreen, maxTimeGreen));
		}

		if (redTimer.hasTimeElapsed()) {
			balloons.add(createBalloon(BalloonType.RED));
			redTimer.reset(MathUtils.random(minTimeRed, maxTimeRed));
		}

		if (smallTimer.hasTimeElapsed()) {
			isSmallBalloon = true;
			balloons.add(createBalloon(BalloonType.SMALL));
			smallTimer.reset(MathUtils.random(minTimeSmall, maxTimeSmall));
		}

		if (blueTimer.hasTimeElapsed()) {
			balloons.add(createBalloon(BalloonType.BLUE));
			blueTimer.reset(MathUtils.random(minTimeBlue, maxTimeBlue));
		}

	}

	/**
	 * Creates a balloon with a Random position.
	 * 
	 * @param bt
	 *           enum type for the balloon to create.
	 */
	public Balloon createBalloon(BalloonType bt) {
		Balloon b = null;
		// Just create a random posX and call the method createBalloon with the position to be set:
		float posX = MathUtils.randomBoolean() ? MathUtils.random(5f * BalloonShooter.scaleX, maxXIzq) : MathUtils
		      .random(minXRight, maxXRight);
		b = createBalloon(bt, new Vector2(posX, posY));
		return b;
	}

	/**
	 * Creates a balloon with a previous position
	 * 
	 * @param bt
	 *           enum type for the balloon to be created
	 * @param position
	 *           for the balloon to be created
	 */
	public Balloon createBalloon(BalloonType bt, Vector2 position) {
		Balloon b = null;
		switch (bt) {
			case RED:
				b = new BalloonRed(position, RED_SPEED);
				break;
			case SMALL:
				b = new BalloonSmall(position, SMALL_SPEED);
				break;
			case BLUE:
				b = new BalloonBlue(position, BLUE_SPEED);
				break;
			case GREEN:
				if (position.x <= 20f*BalloonShooter.scaleX) {
					position.add(20f * BalloonShooter.scaleX - position.x, 0);
				}
				b = new BalloonGreen(position, GREEN_SPEED);
		}
		return b;
	}

	/**
	 * Creates a bonus
	 * 
	 * @return Bonus
	 */
	public Bonus createBonus() {
		float posY = MathUtils.random(MIN_POS_Y_BONUS, MAX_POS_Y_BONUS);
		float posX = MathUtils.randomBoolean() ? -10f*BalloonShooter.scaleX : Gdx.graphics.getWidth() + 10f*BalloonShooter.scaleX;
		return new Bonus(BONUS_SPEED, new Vector2(posX, posY));
	}

	public void increaseBonusSpeed() {
		BONUS_SPEED = BONUS_SPEED * 1.05f;
	}

	/**
	 * Returns the balloons created
	 * 
	 * @return
	 */
	public Array<Balloon> getBalloons() {
		// The array is because sometimes 2 balloons can be created in the same update() call to this class.
		Array<Balloon> a = new Array<Balloon>(balloons);
		balloons.clear();
		return a;
	}

	private void updateTimesEasy() {
		contUpdates++;
		// Values are hard-coded and balanced by testing
		if (contUpdates == 2) {
			if (RED_SPEED < Gdx.graphics.getHeight() / 12.5f)
				RED_SPEED = RED_SPEED * 1.06f;
			if (GREEN_SPEED < Gdx.graphics.getHeight() / 10.0f)
				GREEN_SPEED = GREEN_SPEED * 1.06f;
			if (BLUE_SPEED < Gdx.graphics.getHeight() / 16.0f)
				BLUE_SPEED = BLUE_SPEED * 1.06f;

			if (isSmallBalloon) {
				isSmallBalloon = false;
				if (SMALL_SPEED < Gdx.graphics.getHeight() / 11.5f)
					SMALL_SPEED = SMALL_SPEED * 1.07f;
				if (minTimeSmall > 13f)
					minTimeSmall = minTimeSmall * 0.94f;
				if (maxTimeSmall > 18f)
					maxTimeSmall = maxTimeSmall * 0.92f;
			}
			contUpdates = 0;
		}

		if (minTimeGreen > 4.0f)
			minTimeGreen = minTimeGreen * 0.97f;
		if (maxTimeGreen > 5.0f)
			maxTimeGreen = maxTimeGreen * 0.965f;
		if (minTimeBlue > 12)
			minTimeBlue = minTimeBlue * 0.965f;
		if (maxTimeBlue > 16)
			maxTimeBlue = maxTimeBlue * 0.97f;
		if (minTimeRed > 2.2f)
			minTimeRed = minTimeRed * 0.98f;
		if (maxTimeRed > 2.6f)
			maxTimeRed = maxTimeRed * 0.97f;

	}

	private void updateTimesNormal() {
		contUpdates++;

		if (contUpdates == 2) {
			if (RED_SPEED < Gdx.graphics.getHeight() / 12.5f)
				RED_SPEED = RED_SPEED * 1.065f;
			if (GREEN_SPEED < Gdx.graphics.getHeight() / 9.5f)
				GREEN_SPEED = GREEN_SPEED * 1.065f;
			if (BLUE_SPEED < Gdx.graphics.getHeight() / 15.5f)
				BLUE_SPEED = BLUE_SPEED * 1.065f;

			if (isSmallBalloon) {
				isSmallBalloon = false;
				if (SMALL_SPEED < Gdx.graphics.getHeight() / 11.5f)
					SMALL_SPEED = SMALL_SPEED * 1.08f;
				if (minTimeSmall > 12f)
					minTimeSmall = minTimeSmall * 0.94f;
				if (maxTimeSmall > 16f)
					maxTimeSmall = maxTimeSmall * 0.93f;
			}
			contUpdates = 0;
		}

		if (minTimeGreen > 3.8f)
			minTimeGreen = minTimeGreen * 0.965f;
		if (maxTimeGreen > 4.5f)
			maxTimeGreen = maxTimeGreen * 0.955f;
		if (minTimeBlue > 10)
			minTimeBlue = minTimeBlue * 0.955f;
		if (maxTimeBlue > 15)
			maxTimeBlue = maxTimeBlue * 0.965f;
		if (minTimeRed > 2.1f)
			minTimeRed = minTimeRed * 0.975f;
		if (maxTimeRed > 2.4f)
			maxTimeRed = maxTimeRed * 0.965f;
	}

	private void updateTimesHard() {
		contUpdates++;

		if (contUpdates == 2) {
			if (RED_SPEED < Gdx.graphics.getHeight() / 12.2f)
				RED_SPEED = RED_SPEED * 1.035f;
			if (GREEN_SPEED < Gdx.graphics.getHeight() / 9.2f)
				GREEN_SPEED = GREEN_SPEED * 1.035f;
			if (BLUE_SPEED < Gdx.graphics.getHeight() / 14.2f)
				BLUE_SPEED = BLUE_SPEED * 1.03f;

			if (isSmallBalloon) {
				isSmallBalloon = false;
				if (SMALL_SPEED < Gdx.graphics.getHeight() / 11.5f)
					SMALL_SPEED = SMALL_SPEED * 1.04f;
				if (minTimeSmall > 10f)
					minTimeSmall = minTimeSmall * 0.92f;
				if (maxTimeSmall > 15f)
					maxTimeSmall = maxTimeSmall * 0.94f;

			}
			contUpdates = 0;
		}

		if (minTimeGreen > 3.4f)
			minTimeGreen = minTimeGreen * 0.97f;
		if (maxTimeGreen > 4.0f)
			maxTimeGreen = maxTimeGreen * 0.96f;
		if (minTimeBlue > 8)
			minTimeBlue = minTimeBlue * 0.98f;
		if (maxTimeBlue > 12)
			maxTimeBlue = maxTimeBlue * 0.98f;
		if (maxTimeRed > 1.9f)
			maxTimeRed = maxTimeRed * 0.97f;

	}

}
