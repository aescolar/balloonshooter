package com.autlos.balloonshooter;

import com.autlos.balloonshooter.screens.LogoScreen;
import com.autlos.sgf.GameAbstract;

public class BalloonShooter extends GameAbstract {
	public final static String VER = "1.2.9";
	public final static String TITLE = "BALLOON SHOOTER";
	public static int soundState;
	public Sounds sounds;
	private Assets atlas;

	// If swarm is going to be used:
	public static boolean isSwarmActive;

	// Screens constants:
	public static boolean doingTutorial;
	public static float lblSeparationY;
	public static float lblSeparationX;

	// Properties:
	private Data data;
	// Admob:
	private IActivityRequestHandler myRequestHandler;

	public BalloonShooter(IActivityRequestHandler handler, boolean swarmActive) {
		super(TITLE, VER, false);
		myRequestHandler = handler;
		isSwarmActive = swarmActive;
	}

	public void setAdsVisible(Boolean ads) {
		myRequestHandler.showAds(ads);
	}

	@Override
	public void create() {
		// Constants:
		super.init(480, 800, 320, 480);
		lblSeparationY = 20f * scaleY;
		lblSeparationX = 15f * scaleX;

		doingTutorial = false;

		soundState = Sounds.NOT_MUTED;
		data = new Data();
		sounds = new Sounds();
		atlas = new Assets();

		// Uncomment this to see a log of the Ram used:
		// Gdx.app.log("Java heap", Gdx.app.getJavaHeap()/1024 + "");
		// Gdx.app.log("Java Native", Gdx.app.getNativeHeap()/1024 + "");

		// Change the Screen to skip the logo
//		 setScreen(new MainMenuScreen(this));
		setScreen(new LogoScreen(this));

	}

	public Assets getAtlas() {
		return atlas;
	}

	public Data getData() {
		return data;
	}

	@Override
	public void dispose() {
		super.dispose();
//		menuFont.dispose();
		sounds.dispose();
	}

	@Override
	public void render() {
		super.render();
	}

	@Override
	public void resize(int width, int height) {
		super.resize(width, height);
	}

	@Override
	public void pause() {
		super.pause();
	}

	@Override
	public void resume() {
		super.resume();
	}

	@Override
   protected void detectScale() {
		scaleX = SCALE_X_BIG;
		scaleY = SCALE_Y_BIG;
		if (Math.abs(1 - SCALE_X_BIG) <= Math.abs(1 - SCALE_X_SMALL)) {
//			scaleX = SCALE_X_BIG;
//			scaleY = SCALE_Y_BIG;
			targetSize = TARGET_SIZE.BIG;
		} else {
//			scaleX = SCALE_X_SMALL;
//			scaleY = SCALE_Y_SMALL;
			targetSize = TARGET_SIZE.SMALL;
		}
   }

}
