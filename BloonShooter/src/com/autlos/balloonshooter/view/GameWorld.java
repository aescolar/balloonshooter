package com.autlos.balloonshooter.view;

import com.autlos.balloonshooter.BalloonFactory;
import com.autlos.balloonshooter.BalloonShooter;
import com.autlos.balloonshooter.BalloonFactory.DifficultyMode;
import com.autlos.balloonshooter.models.enemies.Balloon;
import com.autlos.balloonshooter.models.enemies.BalloonBlue;
import com.badlogic.gdx.Gdx;

public class GameWorld extends World {

	private DifficultyMode mode;
	private boolean gameOver;

	// Score before a bonus appear:
	private int scoreForBonus;

	public GameWorld(BalloonShooter game, DifficultyMode mode) {
		super(game);
		this.mode = mode;
		// Sets the difficulty mode to the balloonFactory and get the corresponding highScore:
		balloonFactory = new BalloonFactory(mode);
		shooter.setProjectileConsts(mode);
		if (mode == DifficultyMode.EASY) {
			highScore = Long.parseLong(game.getData().getHighScoresEasy());
			lifes = 5;
		} else if (mode == DifficultyMode.NORMAL) {
			highScore = Long.parseLong(game.getData().getHighScoreNormal());
			lifes = 4;
		} else if (mode == DifficultyMode.HARD) {
			highScore = Long.parseLong(game.getData().getHighScoreHard());
			lifes = 3;
		}

		scoreForBonus = 1000;
		// If it's game over:
		gameOver = false;

		labelScore.setValue(score);
		labelBest.setValue(highScore);

	}

	public void update(float delta) {
		// the super update() method checks the collisions and generic stuff for a game loop
		super.update(delta);
		balloonFactory.update(delta);

		// This way the time between bonus increases as the score gets higher.
		if (score >= 10000) {
			scoreForBonus = 1500;
			if (score >= 100000) {
				scoreForBonus = 2000;
				if (score >= 1000000) {
					scoreForBonus = 2500;
					if (score >= 10000000) {
						scoreForBonus = 5000;
						if (score >= 100000000) {
							scoreForBonus = 10000;
						}
					}
				}
			}
		}

		// Bonus appears every 2000 points, this checks if a bonus has to be created:
		if (contScore != 0 && contScore >= scoreForBonus && !isBonus) {
			isBonus = true;
			contScore = 0;
			bonus = balloonFactory.createBonus();
		}

		// This checks if a bonus has to be removed:
		if (isBonus) {
			if (bonus.getPosition().x + bonus.getWidth() < -11 * BalloonShooter.scaleX
			      || bonus.getPosition().x > Gdx.graphics.getWidth() + 11 * BalloonShooter.scaleX) {
				bonus = null;
				isBonus = false;
			} else {
				bonus.update(delta);
			}
		}

		for (Balloon iterB : balloons) {
			if (iterB.getPosition().y > Gdx.graphics.getHeight()) {
				if (iterB instanceof BalloonBlue) {
					game.getData().addContBlues();
				}
				balloons.removeValue(iterB, false);
				lifes--;
			} else if (iterB.isFinished()) {
				balloons.removeValue(iterB, false);
			} else {
				iterB.update(delta);
			}
		}

		// Add to the array all the balloons created in the factory's update method:
		balloons.addAll(balloonFactory.getBalloons());

		// Check if it's game over:
		gameOver = lifes <= 0;

	}

	public boolean isGameOver() {
		return gameOver;
	}

	public void dispose() {
		super.dispose();
	}

	public DifficultyMode getMode() {
		return mode;
	}

}
