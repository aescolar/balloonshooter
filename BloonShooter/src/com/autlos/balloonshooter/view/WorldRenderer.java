package com.autlos.balloonshooter.view;

import com.autlos.balloonshooter.Assets;
import com.autlos.balloonshooter.BalloonShooter;
import com.autlos.balloonshooter.models.Heart;
import com.autlos.balloonshooter.models.LabelPoints;
import com.autlos.balloonshooter.models.enemies.Balloon;
import com.autlos.sgf.models.BasicProjectile;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class WorldRenderer {

	SpriteBatch batch;
	World world;

	// The shapeRenderer was created to test collisions.
//	ShapeRenderer sr;

	public WorldRenderer(GameWorld world, SpriteBatch batch) {
		this.world = world;
		this.batch = batch;

//		sr = new ShapeRenderer();
//		sr.setColor(Color.RED);

	}

	public void render() {
		batch.begin();

		// Draws every entity:
		for (BasicProjectile iterP : world.getProjectiles()) {
			iterP.draw(batch);
		}
		for (Balloon iterB : world.getBloons()) {
			iterB.draw(batch);
		}
		for (Heart iterH : world.getHearts()) {
			iterH.draw(batch);
		}
		if (world.isBonus()) {
			world.getBonus().draw(batch);
		}
		
		for(LabelPoints iterL : world.getLabelPoints()){
			iterL.draw(batch);
		}

		// Interface:
		world.getLabelBest().draw(batch);
		world.getLabelScore().draw(batch);

		// Hearts:
		float hw = Assets.heart.getRegionWidth();
		float hh = Assets.heart.getRegionHeight();

		float x = Gdx.graphics.getWidth() - (world.getLifes() * ((hw + 5f) * BalloonShooter.scaleX)) - 15f
		      * BalloonShooter.scaleX;
		float y = BalloonShooter.screenHeight - BalloonShooter.lblSeparationY - hh * BalloonShooter.scaleY;
		for (int i = 0; i < world.getLifes(); i++) {
			batch.draw(Assets.heart, x, y, hw / 2, hh / 2, hw, hh, BalloonShooter.scaleX, BalloonShooter.scaleY, 0f);
			x += BalloonShooter.scaleX * (5f + hw);
		}

		// Shooter:
		world.getShooter().draw(batch);
		world.getBtnWeapon().draw(batch);
		world.getBtnSound().draw(batch);

		batch.end();

//		sr.begin(ShapeType.Line);
//		if (world.isBonus) {
//			world.getBonus().drawShape(sr);
//		}
//		for (Heart iterH : world.getHearts()) {
//			iterH.drawShape(sr);
//		}
//		world.getBtnSound().drawShape(sr);
//		world.getBtnWeapon().drawShape(sr);
//
//		for (BasicProjectile iterP : world.getProjectiles()) {
//			iterP.drawShape(sr);
//		}
//		for (Balloon iterB : world.getBloons()) {
//			iterB.drawShape(sr);
//		}
//		sr.end();

	}

	public void dispose() {
		world.dispose();
	}

}
